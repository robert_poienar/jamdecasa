﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CategoryZoneController : MonoBehaviour
{
    // Start is called before the first frame update
    public List<Item> itemList;
    public List<PickUpItem> pickUpItems;
    void Start()
    {
        for(int i=0;i< itemList.Count; i++)
        {
            pickUpItems.Add(transform.GetChild(i).GetComponentInChildren<PickUpItem>());
        }
        foreach(var item in itemList)
        {
            var randomIndex = Random.Range(0, pickUpItems.Count);
            pickUpItems[randomIndex].item = item;
            pickUpItems[randomIndex].itemSpriteRenderer.sprite = item.spriteColored;
            pickUpItems.RemoveAt(randomIndex);
        }
    }

   
}
