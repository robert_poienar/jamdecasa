﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ItemSelected : UnityEvent<Item> { }
public class JobFinished : UnityEvent { }

public class EventManager 
{
    public ItemSelected itemSelected;
    // Start is called before the first frame update
    private static EventManager _instance;
    public JobFinished jobFinished;
    public static EventManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new EventManager();
                _instance.itemSelected = new ItemSelected();
                _instance.jobFinished = new JobFinished();
              
            }
            return _instance;
        }
    }
}
