﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUpItem : MonoBehaviour
{
    public Item item;
    public GameObject Message;
    public Image bubbleSpeech;
    public TMPro.TMP_Text text;
    public SpriteRenderer boxSpriteRenderer;
    public SpriteRenderer itemSpriteRenderer;
    public List<Sprite> ListOfBoxSprites;
    public AudioSource audioSource;
    public bool playSound=false;
    public void Start()
    {
        itemSpriteRenderer.sprite = item.spriteColored;
        boxSpriteRenderer.sprite = ListOfBoxSprites[UnityEngine.Random.Range(0, ListOfBoxSprites.Count)];
        EventManager.Instance.itemSelected.AddListener(HideMessageWithItem);
    }

    public void ShowMessage(bool neededItem)
    {
        Message.SetActive(true);
        if (neededItem)
        {
            bubbleSpeech.color = new Color(19/255f,122/255f,236/255f);
            text.text= "Press \"Space\" to pickup ";
            text.color = Color.white;
        }
        else
        {
            bubbleSpeech.color = new Color(229 / 255f, 228 / 255f, 234 / 255f);
            text.text = "Don't need this item ";
            text.color = Color.black;
        }
    }
    public void HideMessageWithItem(Item item)
    {
        if (this.item == item)
        {
            if (playSound)
            { 
                audioSource.Play();
                playSound = false;
            }
            HideMessage();
        }
    }
    public void HideMessage()
    {
        Message.SetActive(false);
    }
}
