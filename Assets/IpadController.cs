﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class IpadController : MonoBehaviour
{
    private bool isShowing = false;
    public Animator animator;
    public GameObject HomePage;
    public GameObject ListPage;
    public GameObject MapPage;
    public TMPro.TMP_Text timerText;
    
    public GameObject ListElementPrefab;
    public Transform ListHolder;
    public List<GameObject> listGameObject=new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        SetUpList(GameLogicController.Instance.requredItems);
        EventManager.Instance.jobFinished.AddListener(CleanList);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            isShowing = !isShowing;
            Show();
        }
        var timer = GameLogicController.Instance.timer;
        int minute = Mathf.FloorToInt(timer / 60);
        int seconds = Mathf.FloorToInt(timer - minute * 60);
        int miliseconds = Mathf.FloorToInt((timer - Mathf.Floor(timer))*100);
        timerText.text=minute + ":" + seconds + ":" + miliseconds;
    }
    void SetUpList(List<Item> RequiredItems)
    {
        foreach(var item in RequiredItems)
        {
            var element = Instantiate(ListElementPrefab, ListHolder);
            element.GetComponent<ListElementController>().Setup(item);
            listGameObject.Add(element);
        }
        
    }
    void CleanList()
    {
        foreach (var element in listGameObject)
        {
            Destroy(element);
        }
        listGameObject.Clear();
        SetUpList(GameLogicController.Instance.requredItems);
    }

    private void Show()
    {
        if (isShowing)
        {
            animator.SetBool("isShowing", true);
        }
        else
        {
            animator.SetBool("isShowing", false);
        }
    }
    public void MapButtonPressed()
    {
        HomePage.SetActive(false);
        MapPage.SetActive(true);
    }
    public void ListButtonPressed()
    {
        HomePage.SetActive(false);
        ListPage.SetActive(true);
    }
    public void BackButtonPressed()
    {
        HomePage.SetActive(true);
        ListPage.SetActive(false);
        MapPage.SetActive(false);
    }
}
