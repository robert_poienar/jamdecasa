﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class GameLogicController : MonoBehaviour
{
    public static GameLogicController Instance;
    public float timer;
    public List<Item> listOfAllItems;
    public List<Item> requredItems;
    public float score;
    [SerializeField]
    public AudioClip timerAudioClip;

    public AudioSource audioSource;

    bool timerClipPlaying = false;
    internal void ItemSelected(Item selectedItem)
    {
        requredItems.Remove(selectedItem);
    }

    // Start is called before the first frame update
    private void Awake()
    {
        Instance = this;
        timer = 300;
        CreateListItems();
        EventManager.Instance.itemSelected.AddListener(ItemSelected);
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            PlayerPrefs.SetFloat("score", score);
            SceneManager.LoadScene("EndGameScreen");
        }
        if (timer <= 15 && !timerClipPlaying)
        {
            timerClipPlaying = true;
            audioSource.Stop();
            audioSource.PlayOneShot(timerAudioClip);
        }
    }
    public void CompleteAJob()
    {
        score++;
        CreateListItems();
        EventManager.Instance.jobFinished.Invoke();

    }
    private void CreateListItems()
    {
       
        int randomNumberOfItems = Random.Range(1,6);
        int randomCategories = Random.Range(3, Enum.GetNames(typeof(Category)).Length);
        requredItems = new List<Item>();
        List<Item> availableItems = new List<Item>();
        for (int i = 0; i < randomCategories; i++)
        {
            availableItems.AddRange(listOfAllItems.FindAll(item => item.category == (Category)i));
        }
        for (int i = 0; i < randomNumberOfItems; i++)
        {

            var randomItem = availableItems[Random.Range(1, availableItems.Count)];
            availableItems.Remove(randomItem);
            requredItems.Add(randomItem);

        }
    }
}
