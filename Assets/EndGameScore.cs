﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGameScore : MonoBehaviour
{
    // Start is called before the first frame update
    public TMPro.TMP_Text scoreText;
    void Start()
    {
        scoreText.text = "Score: "+PlayerPrefs.GetFloat("score", 0).ToString();
    }

   
}
