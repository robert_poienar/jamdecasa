﻿using UnityEngine;
using UnityEngine.UI;

public class ListElementController: MonoBehaviour
{
    public Image image;
    public TMPro.TMP_Text itemNameText;
    private Item item;
    public void Start()
    {
        EventManager.Instance.itemSelected.AddListener(ItemSelected);
    }
    public void Setup(Item item) {
        this.item = item;
        image.sprite = item.spriteWhite;
        itemNameText.text = item.itemName;
        itemNameText.color = Color.gray;
    }
    public void ItemSelected(Item itemSelected)
    {
        if (item == itemSelected)
        {
            image.sprite = item.spriteColored;
            itemNameText.color = Color.black;
        }
    }
   
}