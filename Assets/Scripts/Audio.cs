﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio : MonoBehaviour
{
    public AudioSource MyAudioSource;
    public AudioClip hoverSound;
    // Start is called before the first frame update
    void Start()
    {
        MyAudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnMouseOver()
    {
        MyAudioSource.PlayOneShot(hoverSound);
    }

    /*OnMouseOver*/
    public void OnMouseEnter(){
        Debug.Log ("Play_Audio");
        MyAudioSource.PlayOneShot(hoverSound);
    }
}
