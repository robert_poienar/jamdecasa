﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Category
{
    TOOL,ELECTRONICS, KITCHENAPPLIANCES, MISC,FOOD
}
[CreateAssetMenu]
public class Item : ScriptableObject
{
    public string itemName;
    public Sprite spriteWhite;
    public Sprite spriteColored;
    public Category category;
}
