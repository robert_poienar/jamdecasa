﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed = 5f;

    public Rigidbody2D rb;
    public Animator animator;
    Vector2 movement;
    public AudioSource audioSource;
    bool isSoundPlaying=false;
    // Update is called once per frame
    void Update()
    {
        // input stuff goes here
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");
        animator.SetFloat("Horizontal", movement.x);
        animator.SetFloat("Vertical", movement.y);
        animator.SetFloat("Speed", movement.sqrMagnitude);
        if (movement.sqrMagnitude > 0 && !isSoundPlaying)
        {
            audioSource.Play();
            isSoundPlaying = true;
        }
        if(movement.sqrMagnitude==0 && isSoundPlaying)
        {
            audioSource.Stop();
            isSoundPlaying = false ;
        }
    }

    void FixedUpdate() 
    {
        // actual movement stuff goes here
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);
    }
}
