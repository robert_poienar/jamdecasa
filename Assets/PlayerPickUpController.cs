﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPickUpController : MonoBehaviour
{
    private Item selectedItem;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (selectedItem != null)
            {
                EventManager.Instance.itemSelected.Invoke(selectedItem);
            }
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision);
        if (collision.gameObject.CompareTag("Item"))
        {
            Debug.Log(collision);
            var pickUpItem = collision.gameObject.GetComponent<PickUpItem>();
            if(GameLogicController.Instance.requredItems.Exists(item => item == pickUpItem.item))
            {
                pickUpItem.ShowMessage(true);
                pickUpItem.playSound = true;
                
            }
            else
            {
                pickUpItem.ShowMessage(false);
            }
            selectedItem = pickUpItem.item;

        }
        if (collision.gameObject.CompareTag("DropOffArea"))
        {
            if (GameLogicController.Instance.requredItems.Count == 0)
            {
                Debug.Log("Finished list");
                GameLogicController.Instance.CompleteAJob();
            }
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Item"))
        {
            var pickUpItem = collision.gameObject.GetComponent<PickUpItem>();
            pickUpItem.HideMessage();
            selectedItem = null;

        }
    }
}
